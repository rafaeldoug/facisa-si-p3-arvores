/**
 * 
 */
package br.cesed.si.p3.arvore;

import java.util.Stack;

import br.cesed.si.p3.arvore.exceptions.ArvoreVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
public class ArvoreBinaria {

	private BinTreeNode raiz;

	/**
	 * 
	 */
	public ArvoreBinaria() {
	}

	/**
	 * @param raiz
	 */
	public ArvoreBinaria(BinTreeNode raiz) {
		this.raiz = raiz;
	}
	
	/**
	 * @param valor
	 */
	public void inserir(int valor) {
		inserir(this.raiz, valor);
	}

	/**
	 * @param raiz
	 * @param valor
	 */
	private void inserir(BinTreeNode raiz, int valor) {

		if (this.raiz == null) {

			this.raiz = new BinTreeNode(valor); // criacao do nodo pai
			System.out.println("Raiz: " + valor);
			
		} else {

			if (valor <= raiz.getValor()) {

				if (raiz.getEsquerda() == null) {
					raiz.setEsquerda(new BinTreeNode(valor));
					System.out.println("Inserido valor " + valor + " � esquerda de " + raiz.getValor());
				} else {
					inserir(raiz.getEsquerda(), valor);
				}
			} else {

				if (raiz.getDireita() == null) {
					raiz.setDireita(new BinTreeNode(valor));
					System.out.println("Inserido valor " + valor + " � direita de " + raiz.getValor());
				} else {
					inserir(raiz.getDireita(), valor);
				}
			}
		}
	}


	
	/**
	 * @throws ArvoreVaziaException
	 */
	public void emOrdem() throws ArvoreVaziaException {
		emOrdem(this.raiz);
	}

	/**
	 * @param raiz
	 * @throws ArvoreVaziaException
	 */
	private void emOrdem(BinTreeNode raiz) throws ArvoreVaziaException {
		
		if (isEmpty()) {
			throw new ArvoreVaziaException("Operacao invalida! Arvore vazia.");
		}

		if (raiz != null) {
			emOrdem(raiz.getEsquerda());
			System.out.println(raiz.getValor() + " ");
			emOrdem(raiz.getDireita());
		}
	}
	
	/**
	 * @throws ArvoreVaziaException
	 */
	public void preOrdem() throws ArvoreVaziaException {
		preOrdem(this.raiz);
	}

	/**
	 * @param raiz
	 * @throws ArvoreVaziaException 
	 */
	private void preOrdem(BinTreeNode raiz) throws ArvoreVaziaException {
		
		if (isEmpty()) {
			throw new ArvoreVaziaException("Operacao invalida! Arvore vazia.");
		}

		if (raiz != null) {
			System.out.println(raiz.getValor() + " ");
			preOrdem(raiz.getEsquerda());
			preOrdem(raiz.getDireita());
		}
	}
	
	public void posOrdem() throws ArvoreVaziaException {
		posOrdem(this.raiz);
	}

	/**
	 * @param raiz
	 * @throws ArvoreVaziaException 
	 */
	private void posOrdem(BinTreeNode raiz) throws ArvoreVaziaException {
		
		if (isEmpty()) {
			throw new ArvoreVaziaException("Operacao invalida! Arvore vazia.");
		}

		if (raiz != null) {
			posOrdem(raiz.getEsquerda());
			posOrdem(raiz.getDireita());
			System.out.println(raiz.getValor() + " ");

		}
	}

	/**
	 * @param raiz
	 * @throws ArvoreVaziaException 
	 */
	public void preOrdemPilha() throws ArvoreVaziaException {
		
		if (isEmpty()) {
			throw new ArvoreVaziaException("Operacao invalida! Arvore vazia.");
		}

		BinTreeNode aux = this.raiz;
		Stack<BinTreeNode> prePilha = new Stack<>();

		if (aux != null) {
			prePilha.push(aux);
			while (!prePilha.isEmpty()) {
				aux = prePilha.pop();
				System.out.println(aux.getValor());
				if (aux.getDireita() != null) {
					prePilha.push(aux.getDireita());
				}
				if (aux.getEsquerda() != null) {
					prePilha.push(aux.getEsquerda());
				}
			}
		}
	}

	/**
	 * @param raiz
	 * @throws ArvoreVaziaException 
	 */
	public void emOrdemPilha() throws ArvoreVaziaException {
		
		if (isEmpty()) {
			throw new ArvoreVaziaException("Operacao invalida! Arvore vazia.");
		}

		BinTreeNode aux = this.raiz;
		Stack<BinTreeNode> emOrdPilha = new Stack<BinTreeNode>();

		while (aux != null || !emOrdPilha.isEmpty()) {
			while (aux != null) {
				emOrdPilha.push(aux);
				aux = aux.getEsquerda();
			}
			if (!emOrdPilha.isEmpty()) {
				aux = emOrdPilha.pop();
				System.out.println(" " + aux.getValor());
				aux = aux.getDireita();
			}
		}
	}

	/**
	 * @return
	 * @throws ArvoreVaziaException 
	 */
	public int getAltura()  {
		return getAltura(this.raiz);
	}

	/**
	 * @param raiz
	 * @return
	 */
	private int getAltura(BinTreeNode raiz) {

		if (raiz == null) {
			return -1;
		} else if (raiz != null && (raiz.getEsquerda() == null && raiz.getDireita() == null)) {
			return 0;
		}

		int altEsq = getAltura(raiz.getEsquerda());
		int altDir = getAltura(raiz.getDireita());

		if (altEsq > altDir) {
			return altEsq + 1;
		} else {
			return altDir + 1;
		}

	}

	/**
	 * @return
	 */
	public int getQtdNos() {
		return getQtdNos(this.raiz);
	}

	/**
	 * @param raiz
	 * @return
	 */
	private int getQtdNos(BinTreeNode raiz) {

		if (raiz == null) {
			return 0;
		}
		int qtdNodeEsq = getQtdNos(raiz.getEsquerda());
		int qtdNodeDir = getQtdNos(raiz.getDireita());
		return qtdNodeDir + qtdNodeEsq + 1;

	}

	/**
	 * @return
	 */
	public int getQtdFolhas() {
		return getQtdFolhas(this.raiz);
	}

	/**
	 * @param raiz
	 * @return
	 */
	private int getQtdFolhas(BinTreeNode raiz) {

		BinTreeNode aux = this.raiz;
		Stack<BinTreeNode> prePilha = new Stack<>();
		int folhas = 0;

		if (aux != null) {
			prePilha.push(aux);
			while (!prePilha.isEmpty()) {
				aux = prePilha.pop();
				if (aux.getDireita() == null && aux.getEsquerda() == null) {
					folhas += 1;
				}
				if (aux.getDireita() != null) {
					prePilha.push(aux.getDireita());
				}
				if (aux.getEsquerda() != null) {
					prePilha.push(aux.getEsquerda());
				}
			}
		}

		return folhas;
	}

	/**
	 * @return
	 */
	public boolean isEmpty() {

		return (this.raiz == null);
	}

	/**
	 * @return
	 */
	public boolean isFull() {

		return Math.pow(2, getAltura()) == getQtdFolhas(); // a quantidade de folhas eh igual ao quadrado da altura da arvore
	}

	/**
	 * @return the raiz
	 */
	public BinTreeNode getRaiz() {
		return raiz;
	}

}
