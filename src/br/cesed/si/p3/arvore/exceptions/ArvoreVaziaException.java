package br.cesed.si.p3.arvore.exceptions;

public class ArvoreVaziaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArvoreVaziaException(String message) {
		super(message);
	}
	
	
	

}
