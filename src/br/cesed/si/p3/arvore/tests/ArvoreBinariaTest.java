/**
 * 
 */
package br.cesed.si.p3.arvore.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.cesed.si.p3.arvore.ArvoreBinaria;
import br.cesed.si.p3.arvore.exceptions.ArvoreVaziaException;

/**
 * @author Rafael Nascimento (Doug)
 *
 */
class ArvoreBinariaTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Insercao de raiz em arvore vazia
	 */
	@Test
	void testInserir1() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(8);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor());
	}

	/**
	 * Insercao de valor em arvore cheia, tomando subarvore da esquerda
	 */
	@Test
	void testInserir2() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);

		/* Execucao */

		binTree.inserir(5);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(5, binTree.getRaiz().getEsquerda().getValor()); // verifica valor na subarvore da esquerda
	}

	/**
	 * Insercao de valor em arvore cheia, tomando subarvore da direita
	 */
	@Test
	void testInserir3() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);

		/* Execucao */

		binTree.inserir(10);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(10, binTree.getRaiz().getDireita().getValor()); // verifica valor na subarvore da direita
	}

	/**
	 * Insercao de valor em arvore com subarvores altura cheias, tomando esquerda da
	 * subarvore de valor 5
	 */
	@Test
	void testInserir4() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5); // subarvore de valor 5
		binTree.inserir(10);

		/* Execucao */

		binTree.inserir(3);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(5, binTree.getRaiz().getEsquerda().getValor());
		assertEquals(3, binTree.getRaiz().getEsquerda().getEsquerda().getValor()); // verifica valor na esquerda da
																					// subarvore valor 5
	}

	/**
	 * Insercao de valor em arvore com subarvores altura cheias, tomando direita da
	 * subarvore de valor 5
	 */
	@Test
	void testInserir5() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5); // subarvore de valor 5
		binTree.inserir(10);
		binTree.inserir(3); // esquerda da subarvore de valor 5

		/* Execucao */

		binTree.inserir(6);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(5, binTree.getRaiz().getEsquerda().getValor());
		assertEquals(6, binTree.getRaiz().getEsquerda().getDireita().getValor());// verifica valor na direita da
																					// subarvore valor 5
	}

	/**
	 * Insercao de valor em arvore com subarvores altura cheias, tomando esquerda da
	 * subarvore de valor 10
	 */
	@Test
	void testInserir6() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5); // subarvore de valor 5
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);

		/* Execucao */

		binTree.inserir(9);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(10, binTree.getRaiz().getDireita().getValor());
		assertEquals(9, binTree.getRaiz().getDireita().getEsquerda().getValor());// verifica valor na esquerda da
																					// subarvore valor 10
	}

	/**
	 * Insercao de valor em arvore com subarvores altura cheias, tomando direita da
	 * subarvore de raiz 10
	 */
	@Test
	void testInserir7() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5); // subarvore de valor 5
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);
		binTree.inserir(9);

		/* Execucao */

		binTree.inserir(12);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(10, binTree.getRaiz().getDireita().getValor());
		assertEquals(12, binTree.getRaiz().getDireita().getDireita().getValor());// verifica valor na direita da
																					// subarvore valor 10
	}

	/**
	 * Insercao de valor em arvore apenas na esquerda
	 */
	@Test
	void testInserir8() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(8);
		binTree.inserir(6);
		binTree.inserir(4);
		binTree.inserir(2);
		binTree.inserir(1);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(6, binTree.getRaiz().getEsquerda().getValor());
		assertEquals(4, binTree.getRaiz().getEsquerda().getEsquerda().getValor());
		assertEquals(2, binTree.getRaiz().getEsquerda().getEsquerda().getEsquerda().getValor());
		assertEquals(1, binTree.getRaiz().getEsquerda().getEsquerda().getEsquerda().getEsquerda().getValor());

	}

	/**
	 * Insercao de valor em arvore apenas na direita
	 */
	@Test
	void testInserir9() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(8);
		binTree.inserir(10);
		binTree.inserir(12);
		binTree.inserir(15);
		binTree.inserir(17);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(10, binTree.getRaiz().getDireita().getValor());
		assertEquals(12, binTree.getRaiz().getDireita().getDireita().getValor());
		assertEquals(15, binTree.getRaiz().getDireita().getDireita().getDireita().getValor());
		assertEquals(17, binTree.getRaiz().getDireita().getDireita().getDireita().getDireita().getValor());

	}

	/**
	 * Insercao de valor em arvore em zigue-zague para esquerda
	 */
	@Test
	void testInserir10() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(10);
		binTree.inserir(6);
		binTree.inserir(8);
		binTree.inserir(7);

		/* Verificacao */

		assertEquals(10, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(6, binTree.getRaiz().getEsquerda().getValor());
		assertEquals(8, binTree.getRaiz().getEsquerda().getDireita().getValor());
		assertEquals(7, binTree.getRaiz().getEsquerda().getDireita().getEsquerda().getValor());

	}

	/**
	 * Insercao de valor em arvore em zigue-zague para direita
	 */
	@Test
	void testInserir11() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(10);
		binTree.inserir(16);
		binTree.inserir(12);
		binTree.inserir(14);

		/* Verificacao */

		assertEquals(10, binTree.getRaiz().getValor()); // verifica valor da raiz
		assertEquals(16, binTree.getRaiz().getDireita().getValor());
		assertEquals(12, binTree.getRaiz().getDireita().getEsquerda().getValor());
		assertEquals(14, binTree.getRaiz().getDireita().getEsquerda().getDireita().getValor());

	}

	/**
	 * Teste de verificacao arvore vazia
	 */
	@Test
	void testIsEmpty1() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Verificacao */

		assertEquals(null, binTree.getRaiz());

		assertTrue(binTree.isEmpty());
	}

	/**
	 * Teste de verificacao arvore nao vazia
	 */
	@Test
	void testIsEmpty2() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);

		/* Verificacao */

		assertEquals(8, binTree.getRaiz().getValor());

		assertFalse(binTree.isEmpty());
	}

	/**
	 * Teste de altura com arvore vazia
	 * 
	 */
	@Test
	void testGetAltura1() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Verificacao */

		int altura = -1;

		assertEquals(altura, binTree.getAltura());

	}

	/**
	 * Teste de altura 0. Apenas com raiz
	 * 
	 */
	@Test
	void testGetAltura2() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);

		/* Verificacao */

		int altura = 0;

		assertEquals(altura, binTree.getAltura());

	}

	/**
	 * Teste de altura
	 * 
	 */
	@Test
	void testGetAltura3() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5);
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);
		binTree.inserir(9);
		binTree.inserir(12);

		/* Verificacao */

		int altura = 2;

		assertEquals(altura, binTree.getAltura());

	}

	/**
	 * Teste de altura de arvore zigue-zague
	 * 
	 */
	@Test
	void testGetAltura4() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(10);
		binTree.inserir(6);
		binTree.inserir(8);
		binTree.inserir(7);

		/* Verificacao */

		int altura = 3;

		assertEquals(altura, binTree.getAltura());

	}

	/**
	 * Teste de quantidade de nos em arvore vazia
	 * 
	 */
	@Test
	void testGetQtdNos1() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Verificacao */

		int qtdNos = 0;

		assertEquals(qtdNos, binTree.getQtdNos());

	}

	/**
	 * Teste de quantidade de nos na arvore
	 * 
	 */
	@Test
	void testGetQtdNos2() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(8);
		binTree.inserir(5);
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);
		binTree.inserir(9);
		binTree.inserir(12);

		/* Verificacao */

		int qtdNos = 7;

		assertEquals(qtdNos, binTree.getQtdNos());

	}

	/**
	 * Teste de quantidade de folhas
	 */
	@Test
	void testqtdFolhas1() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5);
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);
		binTree.inserir(9);
		binTree.inserir(12);

		/* Verificacao */

		int folhas = 4;

		assertEquals(folhas, binTree.getQtdFolhas());

	}

	/**
	 * Teste de quantidade de folhas em arvore degenerada a esquerda
	 */
	@Test
	void testqtdFolhas2() {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		/* Execucao */

		binTree.inserir(8);
		binTree.inserir(6);
		binTree.inserir(4);
		binTree.inserir(2);
		binTree.inserir(1);

		/* Verificacao */

		int folhas = 1;

		assertEquals(folhas, binTree.getQtdFolhas());

	}

	/**
	 * Teste de arvore completa em arvore completa
	 * 
	 * @throws ArvoreVaziaException
	 */
	@Test
	void testIsFull1() throws ArvoreVaziaException {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5);
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);
		binTree.inserir(9);
		binTree.inserir(12);

		/* Verificacao */

		assertTrue(binTree.isFull());

	}

	/**
	 * Teste de arvore completa em arvore nao completa
	 * 
	 * @throws ArvoreVaziaException
	 */
	@Test
	void testIsFull2() throws ArvoreVaziaException {

		/* Cenario */

		ArvoreBinaria binTree = new ArvoreBinaria();

		binTree.inserir(8);
		binTree.inserir(5);
		binTree.inserir(10);
		binTree.inserir(3);
		binTree.inserir(6);

		/* Verificacao */

		assertFalse(binTree.isFull());

	}

}
