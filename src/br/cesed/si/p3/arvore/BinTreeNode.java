/**
 * 
 */
package br.cesed.si.p3.arvore;

/**
 * @author Rafael Nascimento (Doug)
 *
 */

public class BinTreeNode {

	private int valor; // Recebe o objeto do no
	private BinTreeNode esquerda; // Recebe o No anterior
	private BinTreeNode direita; // Recebe o No posterior

	/**
	 * Construtor da Arvore Binaria recebendo um valor
	 * 
	 * @param valor
	 */
	public BinTreeNode(int valor) {
		this.valor = valor;
		this.esquerda = this.direita = null;
	}

	/**
	 * Reseta a arvore
	 */
	public void setNull() {
		this.valor = 0;
		this.esquerda = this.direita = null;
	}

	/**
	 * @return Retorna o valor da raiz
	 */
	public int getValor() {
		return this.valor;
	}

	/**
	 * Ajusta o valor da Raiz
	 * 
	 * @param - Valor da Raiz
	 */
	public void setValor(int valor) {
		this.valor = valor;
	}

	/**
	 * @return Retorna a subarvore na direita da raiz
	 */
	public BinTreeNode getDireita() {
		return this.direita;
	}

	/**
	 * Ajusta a subarvore a direita da raiz
	 * 
	 * @param direita - Subarvore a ser ajustada na direita
	 */
	public void setDireita(BinTreeNode direita) {
		this.direita = direita;
	}

	/**
	 * @return Retorna a subarvore na esquerda da raiz
	 */
	public BinTreeNode getEsquerda() {
		return this.esquerda;
	}

	/**
	 * Ajusta a subarvore a esquerda da raiz
	 * 
	 * @param esquerda - Subarvore a ser ajustada na esquerda
	 */
	public void setEsquerda(BinTreeNode esquerda) {
		this.esquerda = esquerda;
	}

	@Override
	public String toString() {
		return "BinTreeNode [valor=" + valor + "]";
	}

}
